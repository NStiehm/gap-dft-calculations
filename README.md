# GaP DFT calculations



This repo contains the necessary files to reproduce the DFT calculations in my bachelor thesis. However, some modification will be necessary, since these scripts were originally intended to be run on the TU Ilmenau's cluster with the LSF batch job management system.

The batch job scripts are located in the ` /input/ ` subdirectory for each directory in this repo. Edit these scripts to set the variables ` INPUTDIR ` to the absolute path of the ` /input/ ` directory, and ` QEBINDIR ` to the absolute path to the directory containing Quantum ESPRESSO's binaries ` pw.x `, ` bands.x ` and ` epsilon.x `. You might also want to modify the commands relating to openmpi and the overall configuration of the batch job with regards to memory and number of CPU cores.

Note that the per-kpoint, per-band-pair dipole matrix elements and the per-band-pair jDOS are only emitted when using the modified ` epsilon.x ` executable found [here](https://gitlab.com/NStiehm/qe-modified-epsilon/), which might at the same time invalidate the normal intended output of ` epsilon.x `.

All calculations have originally been performed with Quantum ESPRESSO version 6.7.
